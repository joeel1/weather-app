var expect    = require('chai').expect;
var Requester = require('../requester');

var requester               = new Requester();

describe('Test for Kampong Baharu Balakong', function() {

    var json = {};
    it('should return 200', function(done) {
        requester.getWeather("3.0462974", "101.6971655", json, function(tmp) {
            expect(json.statusCode).to.equal(200);
            done();
        });
    });

    it('open weather should return 200', function(done) {
        expect(json.data.cod).to.equal(200);
        done();
    });

    it('id should match 1771304', function(done) {
        expect(json.data.id).to.equal(1771304);
        done();
    });
});

describe('Test for the other side of the world from Kampong Baharu Balakong - Mosquera', function() {

    var json = {};
    it('should return 200', function(done) {
        requester.getHalfwayWeather("3.0462974", "101.6971655", json, function(tmp) {
            expect(json.statusCode).to.equal(200);
            done();
        });
    });

    it('open weather should return 200', function(done) {
        expect(json.data.cod).to.equal(200);
        done();
    });

    it('id should match 3674293', function(done) {
        expect(json.data.id).to.equal(3674293);
        done();
    });
});


describe('Test for combo request', function() {

    var json1     = {};
    var json2     = {};
    var json_resp = {};
    it('should have both current location & halfway data', function(done) {
        requester.getComboWeather("3.0462974", "101.6971655", json1, json2, function(tmp) {
            json_resp = tmp;
            expect(json_resp.current_loc).to.not.be.null;
            expect(json_resp.halfway).to.not.be.null;
            done();
        });
    });

    it('current location id should match 1771304', function(done) {
        expect(json_resp.current_loc.data.id).to.equal(1771304);
        done();
    });

    it('halfway around the world\'s id should match 3674293', function(done) {
        expect(json_resp.halfway.data.id).to.equal(3674293);
        done();
    });
});
