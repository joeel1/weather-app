FROM node:8

# create app directory
WORKDIR /usr/src/app

# solve dependencies
COPY package*.json ./

RUN npm install

# copy sources
COPY . .

EXPOSE 8080

CMD [ "npm", "start" ]