var https   = require('https');


function Requester() {

}

Requester.prototype.getComboWeather = function(lat, lon, data_curr, data_halfway, callback) {
    var self        = this;
    var halfway_lon = lon < 0 ? lon + 180 : lon - 180;

    self.getWeather(lat, lon, data_curr, function(json) {
        self.getWeather(lat, halfway_lon, data_halfway, function(json2) {
            var tmp = { current_loc: json, halfway: json2 };
            callback(tmp);
        });
    });
}

Requester.prototype.getHalfwayWeather = function(lat, lon, json, callback) {
    var halfway_lon = lon < 0 ? lon + 180 : lon - 180;
    this.getWeather(lat, halfway_lon, json, callback);
}

Requester.prototype.getWeather = function(lat, lon, json, callback) {
    var url = "https://api.openweathermap.org/data/2.5/weather?units=metric&lat=" + lat + "&lon=" + lon + "&appid=886a1cf3badfa504bdecaca91ac072e0";
 
    https.get(url, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
      
        resp.on('end', () => {
            json.statusCode = resp.statusCode;
            json.error      = null;
            if (resp.statusCode == 200) {
                json.online = true;
                json.update = Date.now();
                json.data   = JSON.parse(data);
            } else {
                json.online = false;
            }

            callback(json);
        });
     
    }).on("error", (err) => {
        json.responseCode = -1;
        json.error        = err;

        callback(json);
    });    
}


module.exports = Requester;
