var express   = require('express');
var https     = require('https');
var Requester = require('../requester');

var requester               = new Requester();
var router                  = express.Router();
var current_loc_weather     = { type: "current_loc", online: false };
var current_halfway_weather = { type: "halfway", online: false };

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/current', function(req, res, next) {
    var lat   = req.query.lat;
    var lon   = req.query.lon;
    requester.getWeather(lat, lon, current_loc_weather, function(json) {
        res.json(json)
    });
});

router.get('/halfway', function(req, res, next) {
    var lat   = req.query.lat;
    var lon   = req.query.lon;
    requester.getHalfwayWeather(lat, lon, current_halfway_weather, function(json) {
        res.json(json);
        res.end();  
    });
});

router.get('/both', function(req, res, next) {
    var lat         = req.query.lat;
    var lon         = req.query.lon;
    var halfway_lon = lon < 0 ? lon + 180 : lon - 180;

    requester.getComboWeather(lat, lon, current_loc_weather, current_halfway_weather, function(json) {
        res.json(json);
        res.end();  
    });
});


module.exports = router;
